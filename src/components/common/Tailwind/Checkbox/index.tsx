import React, { DetailedHTMLProps, ForwardedRef, forwardRef, InputHTMLAttributes } from 'react';

export type CheckboxProps = Omit<DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>, 'size'> & {
  ref: ForwardedRef<HTMLInputElement>;
  checked?: boolean;
  size?: 'small' | 'normal';
};

const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  ({ checked = false, size = 'normal', disabled, className, ...rest }: CheckboxProps, ref) => {
    const classNames = [
      'appearance-none rounded',
      disabled ? 'bg-ghost-main border-ghost-main' : 'bg-common-white border-ghost-dark hover:border-primary-main',
      checked ? 'bg-primary-main' : 'bg-common-white',
      size === 'small' && 'h-4 w-4',
      size === 'normal' && 'h-5 w-5',
      className,
    ].join(' ');

    return <input ref={ref} type="checkbox" className={classNames} disabled={disabled} checked={checked} {...rest} />;
  },
);

export default Checkbox;
