import { Story, Meta } from '@storybook/react/types-6-0';
import { useState } from 'react';
import Checkbox, { CheckboxProps } from '.';

export default {
  title: 'Example/PureTailwind/Checkbox',
  component: Checkbox,
  parameters: {
    backgrounds: {
      default: 'white',
    },
  },
} as Meta;

const Template: Story<CheckboxProps> = (args) => {
  const [checked, setChecked] = useState(false);

  return (
    <div className="flex">
      <Checkbox {...args} checked={checked} onClick={() => setChecked((ps) => !ps)} />
    </div>
  );
};

export const Default = Template.bind({});
export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};
