import React, { DetailedHTMLProps, HTMLAttributes } from 'react';

type AvailableVariants = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p';

// TODO: THEME should be dynamic (e.g. from context)
const THEME = 'main';

const generateClassName = (_theme: 'main' | 'light' | 'dark' | 'contrastText', variant: AvailableVariants) => {
  const base = 'font-sans font-normal';
  switch (variant) {
    case 'h1':
      return `${base} text-4xl text-primary-main`;
    case 'h2':
      return `${base} text-3xl text-primary-main`;
    case 'h3':
      return `${base} text-2xl text-primary-main`;
    case 'h4':
      return `${base} text-xl text-primary-main`;
    case 'h5':
      return `${base} text-lg text-primary-main`;
    case 'h6':
      return `${base} text-base text-primary-main`;
    case 'p':
      return `${base} text-base text-common-black`;
  }
};

export type TypographyProps = DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement> & {
  element?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p';
  variant?: AvailableVariants;
};

const Typography = ({ element, variant = 'p', ...rest }: TypographyProps) => {
  return React.createElement(element ?? variant, { className: generateClassName(THEME, variant), ...rest });
};

export default Typography;
