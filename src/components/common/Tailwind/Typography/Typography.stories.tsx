import { Story, Meta } from '@storybook/react/types-6-0';
import Typography from '.';

export default {
  title: 'Example/PureTailwind/Typography',
  component: Typography,
} as Meta;

const Template: Story = (args) => <Typography {...args} />;

export const H1 = Template.bind({});
H1.args = {
  variant: 'h1',
  children: 'Heading Level-1',
};

export const H2 = Template.bind({});
H2.args = {
  variant: 'h2',
  children: 'Heading Level-2',
};

export const H3 = Template.bind({});
H3.args = {
  variant: 'h3',
  children: 'Heading Level-3',
};

export const H4 = Template.bind({});
H4.args = {
  variant: 'h4',
  children: 'Heading Level-4',
};

export const H5 = Template.bind({});
H5.args = {
  variant: 'h5',
  children: 'Heading Level-5',
};

export const H6 = Template.bind({});
H6.args = {
  variant: 'h6',
  children: 'Heading Level-6',
};

export const Paragraph = Template.bind({});
Paragraph.args = {
  variant: 'p',
  children: 'Paragraph',
};
