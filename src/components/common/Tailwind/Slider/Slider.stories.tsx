import { Story, Meta } from '@storybook/react/types-6-0';
import { useSlider } from '../../../../hooks/index';
import Slider from '.';

export default {
  title: 'Example/PureTailwind/Slider',
  component: Slider,
} as Meta;

const Template: Story = (args) => {
  const [slider, sliderConfig] = useSlider({
    min: 0,
    max: 100,
    value: 0,
    step: 1,
    label: 'HOOK SLIDER 1',
  });

  return <Slider variant={args?.variant} {...sliderConfig} />;
};

export const Normal = Template.bind({});
Normal.args = {
  variant: 'normal',
};

export const Thin = Template.bind({});
Thin.args = {
  variant: 'thin',
};

export const WithShadow = Template.bind({});
WithShadow.args = {
  variant: 'with-shadow',
};

export const WithInputText = Template.bind({});
WithInputText.args = {
  variant: 'with-input',
};
