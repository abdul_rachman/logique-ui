import React, { useState, useRef, useEffect, memo } from 'react';

type variantSlider = 'normal' | 'dashed' | 'thin' | 'with-input' | 'with-shadow';

interface SliderProps {
  label: string;
  onChange: (value: number) => void;
  value: number;
  classes: string;
  max: number;
  variant: variantSlider;
}

const RangeSlider = memo(({ variant, classes, label, onChange, max = 100, value, ...props }: SliderProps) => {
  const [sliderVal, setSliderVal] = useState(0);
  const [mouseState, setMouseState] = useState(null);
  const [maxValue, setMaxValue] = useState(max);

  useEffect(() => {
    setSliderVal(value);
  }, [value]);

  let sliderRef = useRef(null);
  let active = '#3991e0';
  let inactive = `#c0c0c0`;
  let progress = (sliderVal / maxValue) * 100 + '%';
  let styleInput = { background: `linear-gradient(90deg, ${active} 0% ${progress},${inactive} ${progress} 100%)` };

  const onChangeCallback = (e: React.FormEvent<HTMLInputElement>) => {
    let target = e.target as HTMLTextAreaElement;
    const value = target.value;
    setSliderVal(+value);

    let newBackgroundStyle = `linear-gradient(90deg, ${active} 0% ${progress}%, ${inactive} ${progress}% 100%)`;
    let baz = sliderRef.current;
    if (baz) baz.style.background = newBackgroundStyle;
  };

  useEffect(() => {
    if (mouseState === 'up') {
      onChange(sliderVal);
    }
  }, [mouseState]);

  const handleCounterPlus = () => {
    setSliderVal(sliderVal + 1);
  };

  const handleCounterMinus = () => {
    setSliderVal(sliderVal - 1);
  };

  let normal =
    'slider hover:opacity-100 max-h-2 bg-ghost-dark outline-none bg-opacity-70 w-full rounded-md transition-opacity duration-1000';

  let thin =
    'slider hover:opacity-100 max-h-1 bg-ghost-dark outline-none bg-opacity-70 w-full rounded-md transition-opacity duration-1000';
  let withShadow =
    'slider-thin hover:opacity-100 max-h-1 bg-ghost-dark outline-none bg-opacity-70 w-full rounded-md transition-opacity duration-1000';
  let withInput =
    'slider hover:opacity-100 max-h-1 bg-ghost-dark outline-none bg-opacity-70 w-full rounded-md transition-opacity duration-1000';

  return (
    <div className="w-full">
      {variant === 'with-input' && (
        <div className="flex-row justify-end">
          <button
            onClick={handleCounterPlus}
            type="button"
            className="font-sans mr-1 h-10 px-5 py-1 rounded-md bg-primary-main text-primary-contrastText  focus-within:outline-none hover:bg-primary-dark"
          >
            +
          </button>
          <input
            type="number"
            onChange={onChangeCallback}
            value={sliderVal || 0}
            className="p-2 focus-within:outline-none rounded-sm"
          />
          <button
            onClick={handleCounterMinus}
            type="button"
            className="font-sans ml-1 h-10 px-5 py-1 rounded-md bg-primary-main text-primary-contrastText  focus-within:outline-none hover:bg-primary-dark"
          >
            -
          </button>
        </div>
      )}
      <input
        ref={sliderRef}
        type="range"
        value={sliderVal || 0}
        {...props}
        className={
          variant === 'normal'
            ? normal
            : variant === 'thin'
            ? thin
            : variant === 'with-shadow'
            ? withShadow
            : variant === 'with-input'
            ? withInput
            : ''
        }
        onChange={onChangeCallback}
        onMouseDown={() => setMouseState('down')}
        onMouseUp={() => setMouseState('up')}
        style={styleInput}
      />
    </div>
  );
});

export default RangeSlider;
