import React, { ForwardedRef, ReactElement, useCallback, useState } from 'react';
import Checkbox from '../Checkbox';
import { IconArrow } from '../Icon';

type BaseDropdownProps = {
  ref?: ForwardedRef<HTMLDivElement>;
  className?: string;
  style?: React.CSSProperties;
  variant?: 'default' | 'float';
  options: Array<{ value: string; label: string }>;
  disabled?: boolean;
  placeholder?: string;
};

export type DropdownProps = BaseDropdownProps & {
  multiple: false | undefined;
  value?: string;
  onChange?: (value: string | undefined) => void;
};

export type DropdownMultipleProps = BaseDropdownProps & {
  multiple: true;
  value?: string[];
  onChange?: (values: string[]) => void;
};

type DropdownComponent = {
  (props: DropdownProps): ReactElement | null;
  (props: DropdownMultipleProps): ReactElement | null;
};

const Dropdown: DropdownComponent = React.forwardRef<HTMLDivElement, DropdownProps | DropdownMultipleProps>(
  (
    {
      style,
      className,
      variant = 'default',
      options,
      value,
      placeholder = 'Select Option',
      disabled,
      ...props
    }: DropdownProps | DropdownMultipleProps,
    ref,
  ) => {
    const [open, setOpen] = useState(false);

    const values: undefined | string[] = value ? (Array.isArray(value) ? value : [value]) : undefined;

    // Using multiple & onChange from props object without destructuring
    // to determine props object type, which is either
    // DropdownProps or DropdownMultipleProps
    const handleChange = useCallback(
      (value: string) => {
        if (!props.onChange) return;
        if (props.multiple === true) {
          // Using Set to guarantee uniqueness
          const valuesSet = new Set(values);
          if (valuesSet.has(value)) valuesSet.delete(value);
          else valuesSet.add(value);
          props.onChange(Array.from(valuesSet));
        } else {
          props.onChange(value === values?.[0] ? undefined : value);
        }
      },
      [props.multiple, props.onChange, values],
    );

    const label =
      values && values.length > 0
        ? options
            .filter((item) => values.includes(item.value))
            ?.map((item) => item.label)
            .join(', ') ?? value
        : placeholder;

    const buttonClassName = [
      'block w-full bg-common-white',
      'pl-3 pr-11 h-11',
      'font-sans font-normal text-left text-xs truncate',
      disabled || !(values && values.length > 0) ? 'text-ghost-dark' : '',
      'border',
      disabled ? 'border-ghost-dark' : open ? 'border-primary-main' : 'border-ghost-dark',
      variant === 'default' && open ? 'rounded-t' : 'rounded',
      'hover:border-common-black',
      'focus:outline-none focus:border-primary-main',
    ].join(' ');

    const arrowClassName = [
      'absolute right-2.5 top-1.5 transform pointer-events-none',
      open ? 'rotate-180' : 'rotate-0',
    ].join(' ');

    const optionContainerClassName = [
      'absolute overflow-auto',
      'right-0 left-0 z-50 max-h-64',
      'bg-common-white',
      'border border-primary-main',
      variant === 'default' ? '-mt-0.5 rounded-b border-t-ghost-dark' : '',
      variant === 'float' ? 'mt-1.5 rounded shadow-md' : '',
    ].join(' ');

    const optionClassName = [
      'h-11 px-3',
      'flex items-center cursor-pointer',
      'font-sans font-normal text-xs',
      'hover:bg-ghost-main',
      'focus:outline-none focus:bg-ghost-main',
    ].join(' ');

    return (
      <div ref={ref} className={`relative ${className || ''}`} style={style}>
        <button className={buttonClassName} onClick={() => setOpen((ps) => !ps)} disabled={disabled}>
          {label}
        </button>
        <IconArrow className={arrowClassName} mainColor={disabled ? '#9b9b9b' : open ? '#3991e0' : 'black'} />
        {open && (
          <>
            <ul className={optionContainerClassName}>
              {options.map(({ value, label }) => {
                const selected = !!values?.includes(value);
                return (
                  <li
                    key={value}
                    className={`${optionClassName} ${selected ? 'text-common-black' : 'text-ghost-dark'}`}
                    tabIndex={0}
                    onClick={() => handleChange(value)}
                  >
                    {props.multiple && <Checkbox size="small" className="mr-2.5" checked={selected} />}
                    {label}
                  </li>
                );
              })}
            </ul>
            <div onClick={() => setOpen(false)} className="fixed inset-0 h-full w-full z-10"></div>
          </>
        )}
      </div>
    );
  },
);

export default Dropdown;
