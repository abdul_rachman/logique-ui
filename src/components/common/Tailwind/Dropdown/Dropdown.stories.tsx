import { Story, Meta } from '@storybook/react/types-6-0';
import { useState } from 'react';
import Dropdown, { DropdownProps } from '.';

export default {
  title: 'Example/PureTailwind/Dropdown',
  component: Dropdown,
  argTypes: {
    variant: {
      control: { type: 'radio', options: ['default', 'float'] },
      description: 'Style variant',
      table: {
        defaultValue: { summary: 'default' },
      },
    },
    options: {
      type: { name: 'array', required: true },
    },
    disabled: {
      control: { type: 'boolean' },
    },
    placeholder: {
      control: { type: 'text' },
      defaultValue: 'Select Option',
      table: {
        defaultValue: { summary: 'Select Option' },
      },
    },
  },
  args: {
    variant: 'default',
  },
  parameters: {
    backgrounds: {
      default: 'white',
    },
  },
} as Meta;

const Template: Story<DropdownProps> = (args) => {
  const [values, setValues] = useState<string[]>(['1', '2']);
  const [value, setValue] = useState<string | undefined>('1');

  return (
    <div className="flex">
      <Dropdown {...args} className="mr-1" multiple style={{ width: 256 }} value={values} onChange={setValues} />
      <Dropdown {...args} style={{ width: 256 }} value={value} onChange={setValue} />
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
  variant: 'default',
  options: [
    { value: '1', label: 'Option 1' },
    { value: '2', label: 'Option 2' },
    { value: '3', label: 'Option 3' },
    { value: '4', label: 'Option 4' },
    { value: '5', label: 'Option 5' },
    { value: '6', label: 'Option 6' },
    { value: '7', label: 'Option 7' },
  ],
};

export const Float = Template.bind({});
Float.args = {
  variant: 'float',
  options: [
    { value: '1', label: 'Option 1' },
    { value: '2', label: 'Option 2' },
    { value: '3', label: 'Option 3' },
    { value: '4', label: 'Option 4' },
    { value: '5', label: 'Option 5' },
  ],
};
