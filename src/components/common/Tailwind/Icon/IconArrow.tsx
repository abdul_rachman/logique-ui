import React, { FC } from 'react';
import { IconProps } from '.';

const IconArrow: FC<IconProps> = ({ width = 32, height = 32, mainColor = 'black', ...rest }: IconProps) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 32 32"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      {...rest}
    >
      <title>42951B96-5E8D-4F9D-AE83-BCADF90FA255@0.5x</title>
      <g id="UI-Kit" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="Button" transform="translate(-406.000000, -1601.000000)">
          <g id="Arrow-drop-down-Copy" transform="translate(406.000000, 1601.000000)">
            <polygon id="Shape" fill={mainColor} points="9.33333333 12.3333333 16 21 22.6666667 12.3333333"></polygon>
            <rect id="Bounds" x="0" y="0" width="32" height="32"></rect>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default IconArrow;
