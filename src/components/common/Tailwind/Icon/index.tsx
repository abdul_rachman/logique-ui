import { SVGProps } from 'react';
import IconArrow from './IconArrow';

export type IconProps = SVGProps<SVGSVGElement> & { mainColor?: string };

export { IconArrow };
