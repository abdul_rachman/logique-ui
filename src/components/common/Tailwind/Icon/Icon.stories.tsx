import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps, IconArrow } from '.';

export default {
  title: 'Example/PureTailwind/Icon',
  argTypes: {
    mainColor: {
      type: { name: 'string', required: false },
      control: { type: 'color' },
      description: 'Main color of Icon',
      table: {
        defaultValue: { summary: 'black' },
      },
    },
  },
} as Meta;

const Template: Story<IconProps> = (args) => {
  return (
    <div className="flex">
      <IconArrow {...args} />
    </div>
  );
};

export const Collections = Template.bind({});
