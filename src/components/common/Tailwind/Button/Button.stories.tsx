import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Button from './';

export default {
  title: 'Example/PureTailwind/Button',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story = (args) => <Button onClick={() => console.log('Test...')} {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  children: 'Button',
  color: 'primary',
};

export const Secondary = Template.bind({});
Secondary.args = {
  children: 'Button',
  color: 'secondary',
};

export const Ghost = Template.bind({});
Ghost.args = {
  children: 'Button',
  color: 'ghost',
};
