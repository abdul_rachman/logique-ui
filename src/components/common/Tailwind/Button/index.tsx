import React, { ReactChild, ReactChildren } from 'react';

type ColorTypes = 'primary' | 'secondary' | 'ghost';

interface ButtonTypes {
  color?: ColorTypes;
  children?: ReactChild | ReactChildren; // React.ReactNode;
  onClick: () => void;
  disabled?: boolean;
  // height: string;
  // border: string;
  // radius: string
  // width: string;
}

const Button: React.FC<ButtonTypes> = (props: ButtonTypes) => {
  const { children, color = 'primary', onClick, disabled } = props;

  return (
    <button
      className={`font-sans h-10 px-10 text-${color}-contrastText bg-${color}-main rounded-md focus:shadow-outline focus-within:outline-none hover:bg-${color}-dark transition-colors duration-150`}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Button;
