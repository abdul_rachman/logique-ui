import { Story, Meta } from '@storybook/react/types-6-0';
import { useState } from 'react';
import Pagination, { PaginationProps, PaginationWithPageNumbersProps } from '.';

export default {
  title: 'Example/PureTailwind/Pagination',
  component: Pagination,
  argTypes: {
    variant: {
      controls: { type: 'radio', options: ['simple', 'square', 'round', 'link'] },
      description: 'Style variant',
      table: {
        defaultValue: { summary: 'simple' },
      },
    },
    totalPages: {
      type: { name: 'number', required: true },
    },
    currentPage: {
      type: { name: 'number', required: true },
    },
    displayedPageCount: {
      type: { name: 'number' },
      table: {
        defaultValue: { summary: 5 },
      },
    },
  },
  args: {
    variant: 'simple',
    totalPages: 15,
  },
  parameters: {
    backgrounds: {
      default: 'white',
    },
  },
} as Meta;

const Template: Story = (args: any) => {
  const [currentPage, setCurrentPage] = useState(1);

  return <Pagination currentPage={currentPage} onChange={setCurrentPage} {...args} />;
};

export const Simple = Template.bind({});
export const Square = Template.bind({});
Square.args = {
  variant: 'square',
};
export const Round = Template.bind({});
Round.args = {
  variant: 'round',
};
export const Link = Template.bind({});
Link.args = {
  variant: 'link',
};
