import React, { ReactElement, useCallback, useRef } from 'react';
import Typography from '../Typography';

const DEFAULT_DISPLAYED_PAGE_COUNT = 5;

type BasePaginationProps = {
  className?: string;
  style?: React.CSSProperties;
  totalPages: number;
  currentPage: number;
  onChange: (page: number) => void;
};

export type PaginationProps = BasePaginationProps & {
  variant?: 'simple';
};

export type PaginationWithPageNumbersProps = BasePaginationProps & {
  variant: 'square' | 'round' | 'link';
  displayedPageCount?: number;
};

type PaginationComponent = {
  (props: PaginationProps): ReactElement;
  (props: PaginationWithPageNumbersProps): ReactElement;
};

const Pagination: PaginationComponent = (props: PaginationProps | PaginationWithPageNumbersProps) => {
  const { currentPage, totalPages, onChange } = props;

  const canGoPrev = currentPage > 1;
  const canGoNext = currentPage < totalPages;

  const stableOnChange = useRef(onChange);
  stableOnChange.current = onChange;

  const goPrev = useCallback(() => {
    if (!canGoPrev) return;
    stableOnChange.current(currentPage - 1);
  }, [currentPage, canGoPrev]);

  const goNext = useCallback(() => {
    if (!canGoNext) return;
    stableOnChange.current(currentPage + 1);
  }, [currentPage, canGoNext]);

  const goToFirst = useCallback(() => {
    stableOnChange.current(1);
  }, []);

  const goToLast = useCallback(() => {
    stableOnChange.current(totalPages);
  }, [totalPages]);

  const containerClassName = ['flex items-center font-sans font-normal', props.className ?? ''].join(' ');

  if (props.variant === 'simple') {
    return (
      <div style={props.style} className={containerClassName}>
        <button className={'h-11 w-11 border border-ghost-dark rounded'} onClick={() => goPrev()}>
          {'<'}
        </button>
        <div className="text-ghost-dark text-sm mx-4">
          Page {currentPage} of {totalPages}
        </div>
        <button className={'h-11 w-11 border border-ghost-dark rounded'} onClick={() => goNext()}>
          {'>'}
        </button>
      </div>
    );
  }

  const displayedPageCount = props.displayedPageCount ?? DEFAULT_DISPLAYED_PAGE_COUNT;
  const displayedPages = Array.from({
    length: totalPages < displayedPageCount ? totalPages : displayedPageCount,
  }).map((_, index, arr) => {
    const offset =
      currentPage <= Math.floor(arr.length / 2)
        ? 1
        : Math.min(currentPage - Math.floor(arr.length / 2), totalPages - arr.length + 1);
    return index + offset;
  });

  if (props.variant === 'square') {
    // to keep displayedPages consistent when first or last page displayed
    if (displayedPages[0] === 2) {
      displayedPages.pop();
    }
    if (displayedPages[displayedPages.length - 1] === totalPages - 1) {
      displayedPages.shift();
    }

    return (
      <div style={props.style} className={containerClassName}>
        <button className={'h-11 px-2 mr-3 border border-ghost-dark rounded text-sm'} onClick={() => goToFirst()}>
          First
        </button>
        <button className={'h-11 w-11 border border-ghost-dark rounded'} onClick={() => goPrev()}>
          {'<'}
        </button>
        <div className={'flex items-center h-11 px-2 mx-3 border border-ghost-dark rounded'}>
          {displayedPages[0] !== 1 && (
            <>
              <button className="px-1" onClick={() => stableOnChange.current(1)}>
                1
              </button>
              {displayedPages[0] > 2 && '...'}
            </>
          )}
          {displayedPages.map((page) => (
            <button
              key={page}
              className={`px-1 ${page === currentPage ? 'font-bold' : 'font-normal'}`}
              onClick={() => stableOnChange.current(page)}
            >
              {page}
            </button>
          ))}
          {displayedPages[displayedPages.length - 1] !== totalPages && (
            <>
              {displayedPages[displayedPages.length - 1] < totalPages - 1 && '...'}
              <button className="px-1" onClick={() => stableOnChange.current(totalPages)}>
                {totalPages}
              </button>
            </>
          )}
        </div>
        <button className={'h-11 w-11 border border-ghost-dark rounded'} onClick={() => goNext()}>
          {'>'}
        </button>
        <button className={'h-11 px-2 ml-3 border border-ghost-dark rounded text-sm'} onClick={() => goToLast()}>
          Last
        </button>
      </div>
    );
  }

  if (props.variant === 'round') {
    const hasMorePageIndicator = (
      <div className="h-11 w-11 ml-2.5 rounded-full border border-ghost-dark flex items-center justify-center">
        &#8226;&#8226;&#8226;
      </div>
    );

    return (
      <div style={props.style} className={containerClassName}>
        <button className={'h-11 w-11 border border-ghost-dark rounded-full'} onClick={() => goPrev()}>
          {'<'}
        </button>
        {displayedPages[0] !== 1 && hasMorePageIndicator}
        {displayedPages.map((page) => (
          <button
            key={page}
            className={`h-11 w-11 ml-2.5 rounded-full ${
              page === currentPage ? 'bg-primary-dark text-common-white' : 'border border-ghost-dark'
            }`}
            onClick={() => stableOnChange.current(page)}
          >
            {page}
          </button>
        ))}
        {displayedPages[displayedPages.length - 1] !== totalPages && hasMorePageIndicator}
        <button className={'h-11 w-11 ml-2.5 border border-ghost-dark rounded-full'} onClick={() => goNext()}>
          {'>'}
        </button>
      </div>
    );
  }

  const hasMorePageIndicator = <div className="ml-2.5">...</div>;

  return (
    <div style={props.style} className={containerClassName}>
      <button onClick={() => goPrev()}>{'< Prev'}</button>
      {displayedPages[0] !== 1 && hasMorePageIndicator}
      {displayedPages.map((page) => (
        <button
          key={page}
          className={`ml-2.5 ${page === currentPage ? 'font-bold' : 'font-normal'}`}
          onClick={() => stableOnChange.current(page)}
        >
          {page}
        </button>
      ))}
      {displayedPages[displayedPages.length - 1] !== totalPages && hasMorePageIndicator}
      <button className={'ml-2.5'} onClick={() => goNext()}>
        {'Next >'}
      </button>
    </div>
  );
};

export default Pagination;
