import * as React from 'react';
import { Tab, TabProps } from './Tab';
import { Panel, PanelProps } from './Panel';
import PropTypes from 'prop-types';

interface TabsContext {
  activeTab: string;
  setActiveTab: (label: string) => void;
}

interface TabsComposition {
  Tab: React.FC<TabProps>;
  Panel: React.FC<PanelProps>;
}

const TabsContext = React.createContext<TabsContext | undefined>(undefined);

const Tabs: React.FunctionComponent & TabsComposition = ({ children }) => {
  const [activeTab, setActiveTab] = React.useState('a');

  /**
   * ! Prevent unecessary renders
   */
  const memoizedContextValue = React.useMemo(
    () => ({
      activeTab,
      setActiveTab,
    }),
    [activeTab, setActiveTab],
  );

  return (
    <TabsContext.Provider value={memoizedContextValue}>
      <nav className={`flex flex-col sm:flex-row ${activeTab == 'contained-with-shadow' && 'shadow-md px-2 py-2'}`}>
        {children}
      </nav>
    </TabsContext.Provider>
  );
};

export const useTabs = (): TabsContext => {
  const context = React.useContext(TabsContext);
  if (!context) {
    throw new Error('Component ini harus berada di dalam <Tabs> component as Children Component !');
  }
  return context;
};

Tabs.Tab = Tab;
Tabs.Panel = Panel;

export { Tabs };

Tabs.propTypes = {
  children: PropTypes.node.toString,
};
