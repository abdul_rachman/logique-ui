import * as React from 'react';
import { useTabs } from './Tabs';

export interface PanelProps {
  label: string;
  children: string;
}

export const Panel: React.FC<PanelProps> = (props: PanelProps) => {
  const { label, children } = props;
  const { activeTab } = useTabs();
  return activeTab === label ? <div>{children}</div> : null;
};
