// export * from './Tabs';
import { Tabs } from './Tabs';
import { Tab } from './Tab';
import { Panel } from './Panel';
export { Panel, Tab, Tabs };
