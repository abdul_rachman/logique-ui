import * as React from 'react';
import { useTabs } from './Tabs';

export interface TabProps {
  label: string;
  children: string;
  variant: 'contained' | 'contained-with-shadow' | 'contained-rounded' | 'outlined';
}

export const Tab: React.FC<TabProps> = (props: TabProps) => {
  const { label, children, variant } = props;

  const { setActiveTab, activeTab } = useTabs();

  let contained = `
    text-primary-${activeTab === label ? 'contrastText' : 'contrastTextBlack'}  
    bg-${activeTab === label ? 'primary' : 'ghost'}-main 
    hover:text-primary-light 
    focus:outline-none 
    py-2 max-h-12 w-48 px-8 block
    font-medium  
    font-sans  
  `;

  let outlined = `
    text-${activeTab === label ? 'primary-main' : 'ghost-dark'} 
    py-1 max-h-12 w-48 px-8 block 
    hover:text-primary-light 
    focus:outline-none  
    font-medium 
    border-${activeTab === label ? 'primary-main' : 'ghost-dark'} 
    border-b-2
    font-sans
  `;

  let containedWithShadow = `
    text-${activeTab === label ? 'primary-contrastText' : 'ghost-dark'} 
    ${activeTab === label ? 'bg-primary-main' : ''}  
    rounded-md 
    py-2 max-h-10 w-48 px-8 block 
    hover:text-primary-light 
    focus:outline-none 
    font-medium 
    font-sans
  `;

  let containedRounded = `
    text-${activeTab === label ? 'primary-contrastText' : 'ghost-dark'} 
    ${activeTab === label ? 'bg-primary-main' : ''}  
    rounded-3xl 
    py-2 max-h-10 w-48 px-8 block 
    hover:text-primary-light  
    focus:outline-none 
    font-sans
    font-medium `;
  return (
    <button
      className={
        variant === 'contained'
          ? contained
          : variant === 'outlined'
          ? outlined
          : variant === 'contained-with-shadow'
          ? containedWithShadow
          : variant === 'contained-rounded'
          ? containedRounded
          : null
      }
      onClick={() => setActiveTab(label)}
    >
      {children}
    </button>
  );
};
