import { Story, Meta } from '@storybook/react/types-6-0';
import { Tabs } from '.';

export default {
  title: 'Example/PureTailwind/Tabs',
  component: Tabs,
} as Meta;

const Template: Story = (args) => {
  const { variant } = args;
  return (
    <Tabs>
      <Tabs.Tab label="a" variant={variant}>
        Tab A
      </Tabs.Tab>
      <Tabs.Tab label="b" variant={variant}>
        Tab B
      </Tabs.Tab>
      <Tabs.Tab label="c" variant={variant}>
        Tab C
      </Tabs.Tab>

      <Tabs.Panel label="a">.</Tabs.Panel>
      <Tabs.Panel label="b">.</Tabs.Panel>
      <Tabs.Panel label="c">.</Tabs.Panel>
    </Tabs>
  );
};

export const Contained = Template.bind({});
Contained.args = {
  variant: 'contained',
};

export const Outlined = Template.bind({});
Outlined.args = {
  variant: 'outlined',
};

export const ContainedWithShadow = Template.bind({});
ContainedWithShadow.args = {
  variant: 'contained-with-shadow',
};

export const ContainedRounded = Template.bind({});
ContainedRounded.args = {
  variant: 'contained-rounded',
};
