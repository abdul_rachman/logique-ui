## Goal : user dapat merubah panel konten dengan klik salah satu grup button. Hanya 1 panel yang muncul, based on button yang di pilih.

--- 

### Compound component yang akan di buat terdiri dari 3 parts :

- Tabs - Parent component as store, keeping state.
- Tab - A Component yang memudahkan user melakukan action pilih Panel konten yang di inginkan. (as like Action)
- Panel - A component of content yang menerima state dari action yang di pilih. (as like Reducer)