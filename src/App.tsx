import React, { useState, useRef, useCallback, useEffect, memo } from 'react';
import { useSlider } from './hooks/index';
import RangeSlider from './components/common/Tailwind/Slider/index';

function App() {
  //#
  const [slider, sliderConfig] = useSlider({
    min: 0,
    max: 100,
    value: 0,
    step: 1,
    label: 'HOOK SLIDER 1',
  });

  return (
    <div>
      <header className="flex justify-center"></header>

      <div className="h-screen flex justify-center items-center m-8">
        <RangeSlider variant="with-input" {...sliderConfig} />
      </div>

      <article className="flex justify-center mt-8">
        <div>
          <br />
          <br />
          <nav className="flex flex-col sm:flex-row shadow-md px-2 py-2">
            <button className="text-primary-contrastText bg-primary-main rounded-md py-2 max-h-10 w-48 px-8 block hover:text-primary-light focus:outline-none font-medium ">
              Tab 1
            </button>
            <button className="text-primary-contrastTextBlack bg-ghost-main py-2 px-6 max-h-12 w-48 block hover:text-primary-light focus:outline-none ">
              Tab 2
            </button>
            <button className="text-primary-contrastTextBlack bg-ghost-main py-2 px-6 max-h-12 w-48 block hover:text-primary-light focus:outline-none">
              Tab 3
            </button>
          </nav>
        </div>
      </article>
    </div>
  );
}

export default App;
