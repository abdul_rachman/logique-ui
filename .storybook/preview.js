import '../src/index.css';
// import { addDecorator } from '@storybook/react';

// addDecorator((story) => (

//     {story()}
  
// ));

export const parameters = {
  backgrounds: {
    default: 'light',
    values: [
      {
        name: 'white',
        value: '#fff',
      },
      {
        name: 'light',
        value: '#f5f5f5',
      },
      {
        name: 'dark',
        value: '#333333',
      },
    ]
  }
}
