module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',    
    // 'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      generators: false,
      jsx: true,
      legacyDecorators: true,
      objectLiteralDuplicateProperties: false,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'no-useless-catch': 'off',
    'import/no-useless-path-segments': 'off',
    'no-useless-escape': 'off',
    'import/no-anonymous-default-export': 'off',
    'import/extensions': 'off',
    'react/display-name': 'off',
    'react/no-did-mount-set-state': 'off',
    'react/no-did-update-set-state': 'off',
    'react/no-unsafe': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/self-closing-comp': 'off',
    'jest/consistent-test-it': 'off',    
    // 'no-unused-vars': ['error', { vars: 'all', args: 'after-used', ignoreRestSiblings: true }],
  },
};
