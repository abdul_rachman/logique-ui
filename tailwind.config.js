const flattenColorPalette = require('tailwindcss/lib/util/flattenColorPalette').default;
// const defaultTheme = require('tailwindcss/defaultTheme');
// const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/components/common/Tailwind/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      primary: {
        light: '#77c1ff',
        main: '#3991e0',
        dark: '#0064ae',
        contrastText: '#ffffff',
        contrastTextBlack: '#000000',

      },
      secondary: {
        light: '#6395cb',
        main: '#2e679a',
        dark: '#003d6b',
        contrastText: '#ffffff',
      },

      error: {
        light: '#bf334c',
        main: '#B00020',
        dark: '#7b0016',
        contrastText: '#000000',
      },
      warning: {
        light: '#ffee33',
        main: '#ffea00',
        dark: '#b2a300',
        contrastText: '#000000',
      },
      success: {
        light: '#91ff35',
        main: '#76ff03',
        dark: '#52b202',
        contrastText: 'white',
      },
      common: {
        white: '#ffffff',
        black: 'black',
        // ghost: '#f3f3f3'
      },
      ghost: {
        light: '#ffffff',
        main: '#f3f3f3',
        dark: '#c0c0c0',
        contrastText: 'black',

      }

    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    // require('@tailwindcss/forms'),
    // https://github.com/tailwindlabs/tailwindcss/pull/560#issuecomment-670045304
    ({ addUtilities, e, theme, variants }) => {
      const colors = flattenColorPalette(theme('borderColor'));
      delete colors['default'];

      const colorMap = Object.keys(colors)
        .map(color => ({
          [`.border-t-${color}`]: {borderTopColor: colors[color]},
          [`.border-r-${color}`]: {borderRightColor: colors[color]},
          [`.border-b-${color}`]: {borderBottomColor: colors[color]},
          [`.border-l-${color}`]: {borderLeftColor: colors[color]},
        }));
      const utilities = Object.assign({}, ...colorMap);

      addUtilities(utilities, variants('borderColor'));
    },
  ],
  fontFamily: {
    // display: ['Cairo'],
    'sans': ['Cairo', 'sans-serif'],   
  }
};

//@ Docs : https://tailwindcss.com/docs/configuration
