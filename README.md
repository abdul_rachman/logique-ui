### For first time adding story-book in your web app

`npx sb init`

### Then.. Start the component explorer on port 6006:

`yarn storybook`

### Want to deploy/build:

`yarn build-storybook`

`npx netlify deploy`

### Notes

- [x] Try to use Color Pallete System default inside Tailwind Config
- [x] Try to prevent use React.FC, if still make sense to create Type of Component your self
- [x] Adding prefix `Tw` for every Props inside reusable component - example : `TwButtonProps`

### Info

- [Link Abstract UI-Kit](https://app.abstract.com/projects/915d8e72-8075-4d22-8ecc-a5f4ecce93d1/branches/658d49d6-74ea-4b66-b070-821fa06a61c8/commits/608aae06fbe7bc3cb651c02208309686b72aa069/files/93e8cf6d-d289-4674-8522-d2da2a3b4360/layers/CD8DDE1E-CACD-4472-BF16-EEFE81B5170D)
- For a while commit using `yarn commit` if you want get `pre-commit-message`. Next, will be deprecated

- Link Docs Storybook : https://logique-ui.netlify.app/

- If you want see viewer of Tailwind (Based on what we config in taiwind.config.js) : `npx tailwind-config-viewer`

### To Do List

- [x] Button
- [x] Typography
- [x] Checkbox
- [x] Switch
- [x] Radio
- [x] folder deploy build of Storybook is still using `findDOMNode` (in react 17+ is deprecated). So for a while "eslint --ignore-pattern '!.\*.tsx'" haven't implemented yet
